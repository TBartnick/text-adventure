# Created By:       Tyler Bartnick
# Creation Date:    9/6/2017
# Updated By:       <insert name here>
# Update Date:      <insert update date here>
# Team Name:        <insert team name here>
# Course:           CSC 320 - Game Programming for Computer Scientists
# Professor:        Dr. Pham
# Due Date:         TBA

import sys
import time
import pickle

import Player

def slow_print(string):
    """ slow_print prints text to the console is if a user were typing.

    This functionality is experiemental as it may detract from user
    experience, although the goal of the method is to introduce an improved
    user experience.
    """
    for i in string:
        sys.stdout.write(i)
        time.sleep(0.035)
    sys.stdout.write("\n")

def prompt_user(choices = []):
    while True:
        try:
            choice = str(input("> ")).lower()
        except ValueError:
            print("Unable to process your request. Please try again.")
        if choice not in choices:
            print("Unable to process your request. Please try again.")
            continue
        else:
            break
    return choice

def main_menu():
        """ The main menu.

        Calling this method displays the main menu to the user.
        """
        choices = ["n", "c", "q"]
        
        slow_print("Main Menu")
        slow_print("N - new game")
        slow_print("C - continue game")
        slow_print("Q - quit game")
        slow_print("Please enter your choice")

        choice = prompt_user(choices)
        choice = choice.lower()
        return choice

def create_new_game():
    print("What is your name?")
    name = get_user_string_input()
    player = Player.Player(name)
    return player

def get_user_string_input():
    try:
        userInput = str(input("> ")).lower()
    except Exception as e:
        print(e)

    return userInput

def save_game(toSave):
        choices = ['y', 'n']
        slow_print("Are you sure you would like to save your game? [y/n]")
        choice = prompt_user(choices)
        choice = choice.lower()
        if choice == 'y':
            with open('game_save.p', 'wb') as saveFile:
                slow_print("Saving...")
                pickle.dump(toSave, saveFile)
                time.sleep(1)
                slow_print("Save complete!")

def load_game():
    choices = ['y', 'n']
    slow_print("Are you sure you would like to load your previously saved game? [y/n]")
    choice = prompt_user(choices)
    choice = choice.lower()
    if choice == "y":
        with open('game_save.p', 'rb') as saveFile:
            slow_print("Loading your previously saved game...")
            player = pickle.load(saveFile)
            time.sleep(1)
            slow_print("Load successful!")
            return player
    elif choice == "n":
        return False
