#!/usr/bin/python3

# Created By:       Tyler Bartnick
# Creation Date:    9/6/2017
# Updated By:       <insert name here>
# Update Date:      <insert update date here>
# Team Name:        <insert team name here>
# Course:           CSC 320 - Game Programming for Computer Scientists
# Professor:        Dr. Pham
# Due Date:         TBA

import enum

class CharacterType(enum.Enum):
    PLAYER = 1
    ENEMY = 2
    BOSS = 3
    NPC = 4
