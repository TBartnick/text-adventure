#!/usr/bin/python3

# Created By:       Tyler Bartnick
# Creation Date:    9/6/2017
# Updated By:       <insert name here>
# Update Date:      <insert update date here>
# Team Name:        <insert team name here>
# Course:           CSC 320 - Game Programming for Computer Scientists
# Professor:        Dr. Pham
# Due Date:         TBA

import os
import sys
import pickle
import pathlib

import Game
import Player

from Utilities import *

def main():
    # load game if one exists already
    # user can elect to start a new game instead of loading one
    while True:
        userChoice = main_menu()
        
        if userChoice == "n":
            player = create_new_game()
            break

        elif userChoice == "c":
            pathName = os.getcwd()
            pathToSaveFile = pathlib.Path(pathName + "/game_save.p")
            
            if pathToSaveFile.is_file():
                player = load_game()
                if player == False:
                    player = create_new_game()
            else:
                print("Previous save file not found... Beginning new game.")
                player = create_new_game()
            break

        elif userChoice == "q":
            print("Goodbye!")
            sys.exit(0)
            break
        
    game = Game.Game(player)

    # start game instance
    game.start_game()

if __name__ == "__main__":
    main()
    sys.exit(0)
