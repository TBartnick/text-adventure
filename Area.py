#!/usr/bin/python3

# Created By:       Tyler Bartnick
# Creation Date:    9/6/2017
# Updated By:       <insert name here>
# Update Date:      <insert update date here>
# Team Name:        <insert team name here>
# Course:           CSC 320 - Game Programming for Computer Scientists
# Professor:        Dr. Pham
# Due Date:         TBA

class Area(object):
    def __init__(self, name = "", enemies = []):
        self.name = name
        self.enemies = enemies