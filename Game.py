# Created By:       Tyler Bartnick
# Creation Date:    9/6/2017
# Updated By:       <insert name here>
# Update Date:      <insert update date here>
# Team Name:        <insert team name here>
# Course:           CSC 320 - Game Programming for Computer Scientists
# Professor:        Dr. Pham
# Due Date:         TBA

import sys
import time

import Player
from Utilities import *

class Game(object):
    """ Global Game object

    All interactions within the game will be controlled through the Game class.
    """

    def __init__(self, player1 = None):
        """ The Game object

        The Game object will have support for saving information (save game).
        Constructing the Game object in such a way will allow for dynamic
        context switching when initializing a Game instance. Thus, we will be
        able to simply and easily create a save game feature.
        """
        if player1 == None:
            self.player = Player.Player("Player 1")
        else:
            self.player = player1

        self.commands = ["help", "inventory", "directions", "examine", "take", "drop"]

    def start_game(self):
        print("+-" * 38 + "+")
        slow_print("Game has started!")
        print("+-" * 38 + "+")
        slow_print("Player is named: " + self.player.name)
        slow_print("Player health is: " + str(self.player.health))
        slow_print("Player inventory has " + str(len(self.player.inventory)) + " items.")
        slow_print("Player character type is: " + str(self.player.characterType.value) + ", which is a(n) " + self.player.characterType.name + ".")
        print("+-" * 38 + "+")

        print("Would you like to save your game?")
        choice = prompt_user(["y", "n"])
        if choice == "y":
            save_game(self.player)
        else:
            pass
