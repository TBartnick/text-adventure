# Created By:       Tyler Bartnick
# Creation Date:    9/6/2017
# Updated By:       <insert name here>
# Update Date:      <insert update date here>
# Team Name:        <insert team name here>
# Course:           CSC 320 - Game Programming for Computer Scientists
# Professor:        Dr. Pham
# Due Date:         TBA

import Character
from CharacterType import CharacterType

class Player(Character.Character):
    """ Derived class for the player

    All player interactions for the game will be controlled in the game
    instance's global player class (i.e. inventory actions, player hit, player
    attack, etc.).
    """
    
    def __init__(
        self, name = "Player1",
        characterType = CharacterType.PLAYER,
        level = None):
        """ Player class constructor

        The constructor function defines all member of the Player class.
        """
        super().__init__()
        self.name = name
        self.inventory = []
        self.level = level
        self.characterType = characterType
