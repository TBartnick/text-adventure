# Instructions

Run by simply typing `python main.py` into the terminal.

# Notes

Depending on your system and what version(s) of python is installed, you may need to type `python3 main.py` instead. This project was built using Python >= v3.6.1.