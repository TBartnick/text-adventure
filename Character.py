# Created By:       Tyler Bartnick
# Creation Date:    9/6/2017
# Updated By:       <insert name here>
# Update Date:      <insert update date here>
# Team Name:        <insert team name here>
# Course:           CSC 320 - Game Programming for Computer Scientists
# Professor:        Dr. Pham
# Due Date:         TBA

import CharacterType

class Character(object):
    """ Base class for all characters within the game. """
    def __init__(self):
        self.health = 100
        